import Vue from 'nativescript-vue';

Vue.registerElement("Mapbox", () => require("nativescript-mapbox").MapboxView)

import HelloWorld from './components/HelloWorld';


import './styles.scss';

// Uncomment the following to see NativeScript-Vue output logs
//Vue.config.silent = false;

new Vue({

  render: h => h(HelloWorld),

}).$start();
